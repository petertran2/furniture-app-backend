class CreateBeds < ActiveRecord::Migration[6.0]
  def change
    create_table :beds do |t|
    	t.string :image, null: false
    	t.string :name, null: false
    	t.integer :price, null: false
    	t.integer :quantity, null: false
    	t.text :desc, null: false
    	t.timestamps
    end
  end
end
