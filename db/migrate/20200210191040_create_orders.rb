class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
    	t.string :name, null: false
    	t.string :email, null: false
    	t.string :address, null: false
    	t.string :city, null: false
    	t.string :state, null: false
    	t.integer :zip, null: false
    	t.integer :cardnumber, null: false
    	t.integer :total, null: false
    	t.string :cart, null: false
    	t.timestamps
    end
  end
end
