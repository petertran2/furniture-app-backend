# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Sofa.destroy_all
Bed.destroy_all
Chair.destroy_all
Other.destroy_all
Table.destroy_all

Sofa.create!(image:'https://pt-furniture-backend.herokuapp.com/sofas/soft-interior-sofa.jpg', name:'Glamour Soft Interior Extended Sofa Set', price: 2000, quantity: 2, desc: 'Bring flexible, comfortable seating for the whole family to your living space with the Glamour Soft Interior Extended multi-piece fabric sectional sofa set. You can arrange the modular pieces numerous ways for a custom look, and the solid-wood frame construction makes the set sturdy and durable.')

Sofa.create!(image:'https://pt-furniture-backend.herokuapp.com/sofas/pink-white-sofa.jpg', price: 1500, name: 'Fiery Pink with White Background Sofa', quantity: 0, desc: 'Millennial or not, you are going to love this on-trend Fiery Pink with White Background sofa by us. A leaning back design on a fiercely painted background is softened with supple fabric, button tufting, and matching smooth curving armrests. Introduce the style-forward furniture to your living room, office, or den for instant glam points and a perfect place to relax.')

Sofa.create!(image: 'https://pt-furniture-backend.herokuapp.com/sofas/churchill-sofa.jpg', price: 1000, name: 'Brown Churchill Leather Sofa', quantity: 12, desc: 'This beautiful Brown Churchill Leather sofa adds a sense of class and sophistication to your living space. The top-grain leather and the brass nailhead trimmings elegantly adorn the kiln-dried wood frame for a beautiful look. With resilient density foam separated into a three-part back, this dark brown sofa is also very comfortable.')

Bed.create!(image: 'https://pt-furniture-backend.herokuapp.com/beds/palm-bed.jpg', price: 150, name: 'Red Velvet Upholstered Bed', quantity: 2, desc: 'Relax in contemporary style with this plush, upholstered bed from us. The rubberwood and MDF frame is padded with polyurethane foam and upholstered with red velvet polyester for lasting style. Grid-tufted details add depth and modern style to the supportive headboard.')

Bed.create!(image: 'https://pt-furniture-backend.herokuapp.com/beds/classy-bed.jpg', price: 500, name: 'Classy Mid-Century Modern Platform Bed', quantity: 0, desc: 'Cultivate a vintage chic vibe in your bedroom with this bed. Built with a low platform set on top of angled, tapered legs, the bed exhibits all the hallmarks of stylish mid-century modern design. The wrap-around headboard features panels of exquisitely woven synthetic rattan that create a cozy, airy ambiance.')

Chair.create!(image: 'https://pt-furniture-backend.herokuapp.com/chairs/office-chair.jpg', price: 150, name: 'Mid-Back Faux Leather Office Chair', quantity: 2, desc: 'The Mid-Back Faux Leather Office Chair features an attractive finished base and arms with all the ergonomic comfort of a modern office chair. Alongside built-in lumbar support, this chair features one-touch pneumatic seat height adjustment, locking tilt control, adjustable tilt tension and dual wheel carpet casters.')

Chair.create!(image: 'https://pt-furniture-backend.herokuapp.com/chairs/chair-sepia.jpg', price: 200, name: 'Mid-Century Lounge Chair', quantity: 0, desc: 'Welcome plush comfort into your home with the vintage style lounge chair. Constructed of durable rubberwood for exceptional durability and stable comfort, this chair is upholstered in distressed brown faux leather or polyester fabric to give it a vintage feel. Perfect for a modern or minimal interior, it features smooth angular solid wood frame in dark walnut finishing for a nod to mid-century modern design that exudes warmth.')

Other.create!(image: 'https://pt-furniture-backend.herokuapp.com/others/white-pitcher-bedside.jpg', price: 150, name: 'One-drawer Oval Wood Bedside', quantity: 2, desc: 'Designed to evoke traditional style with a modern edge, the bedside from us is a versatile addition to any living space. The unique oval shape makes this piece visually appealing and is constructed from Poplar wood. An ample storage drawer with wooden glides feature French dovetail drawer construction. The storage makes this piece an excellent companion for your bedroom set, or even in the entry way.')

Table.create!(image: 'https://pt-furniture-backend.herokuapp.com/tables/yellow-table.jpg', price: 450, name: 'Cosmopolitan Amari Oval Faux Yellow Table', quantity: 2, desc: 'Choose a chill look for your dining space with this faux yellow dining table. An oval tabletop offers a softer look and makes it easy to get around this four-person table. A yellow finish with metal on the four legs keeps this table light and neutral with versatile contemporary appeal.')
