Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api do
  	namespace :v1 do
  		resources :sofas, only: [:index]
  		resources :beds, only: [:index]
  		resources :chairs, only: [:index]
  		resources :others, only: [:index]
  		resources :tables, only: [:index]
  		resources :orders, only: [:create]
  	end
  end
end
