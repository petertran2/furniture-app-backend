module Api
	module V1
		class TablesController < ApplicationController
			def index
				@tables = Table.all
				render json: @tables
			end
		end
	end
end
