module Api
	module V1
		class BedsController < ApplicationController
			def index
				@beds = Bed.all
				render json: @beds
			end
		end
	end
end
