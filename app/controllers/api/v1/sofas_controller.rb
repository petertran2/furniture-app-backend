module Api
	module V1
		class SofasController < ApplicationController
			def index
				@sofas = Sofa.all
				render json: @sofas
			end
		end
	end
end
