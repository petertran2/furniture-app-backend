module Api
	module V1
		class OrdersController < ApplicationController
			def create
				@order = Order.new(order_params)
				if @order.save
					render json: @order, status: :created
				else
					render json: @order.errors.full_messages, status: :unprocessable_entity
				end
			end
			
			private
			
			def order_params
				params.require(:order).permit(:name, :email, :address, :city, :state,
					:zip, :cardnumber, :total, :cart)
			end
		end
	end
end
