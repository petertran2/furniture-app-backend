module Api
	module V1
		class ChairsController < ApplicationController
			def index
				@chairs = Chair.all
				render json: @chairs
			end
		end
	end
end
