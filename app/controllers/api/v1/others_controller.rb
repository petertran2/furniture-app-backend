module Api
	module V1
		class OthersController < ApplicationController
			def index
				@others = Other.all
				render json: @others
			end
		end
	end
end
