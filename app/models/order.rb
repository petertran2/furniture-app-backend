class Order < ApplicationRecord
	validates :name, :email, :address, :city, :state, :zip, :cardnumber, :total,
		:cart, presence: true
end
