class Sofa < ApplicationRecord
	validates :image, :name, :price, :quantity, :desc, presence: true
end
