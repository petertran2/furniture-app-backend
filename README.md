# Furniture App Back-End

This web app for furniture products is a Ruby on Rails app that contains listings of different kinds of furniture for an e-commerce shop, ranging from sofas to chairs to beds. Configured as an API, it sends listings data to its corresponding front-end mobile app written in React Native to display the furniture available for sale. It also receives customers' orders from the mobile app and stores them in a PostgreSQL database. The back-end app has been deployed to Heroku ([link](https://pt-furniture-backend.herokuapp.com/)).

For more information about the mobile app, please visit [its repository](https://gitlab.com/petertran2/furniture-app).

## Technologies

- React Native (front end)
- React Navigation
- Vector Icons
- Formik
- Ruby on Rails (back end)
- PostgreSQL
- JSON
- Heroku

### Author

Peter Tran